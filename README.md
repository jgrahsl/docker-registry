# Simple docker registry

## TLS

The registry itself is running as ordinary http service. This repo relies on a running jrcs/letsencrypt-nginx-proxy-companion together with a nginx reverse proxy.

## Credentials

Create credentials file `auth/htpasswd`

`docker run --entrypoint htpasswd  registry:2.6.2 -Bbn <username> <password> > auth/htpasswd`

Note that this works only with version `2.6.2` as htpasswd was removed from the runtime container in higher versions

## Login

To use the registry you need to:

`docker login <hostname>`

## Push/Pull

`docker pull <hostname>/<repo>`

`docker push <hostname>/<repo>`
